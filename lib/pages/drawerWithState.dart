import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tcg_companion/pages/userRegister.dart';
import 'package:tcg_companion/pages/user_sign_in.dart';

import '../main.dart';
import 'UserRegisterFirst.dart';
import 'adminPage.dart';

class stateDrawer extends StatefulWidget {
  @override
  _stateDrawerState createState() => _stateDrawerState();
}

class _stateDrawerState extends State<stateDrawer> {
  User currentUser;

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.authStateChanges().listen((User user) {
      setState(() {
        if (user == null) {
          print('User is currently signed out!');
        } else {
          currentUser = user;
          print('User is signed in!');
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
          child: ListView.builder(
            itemCount: 5,
            itemBuilder: (BuildContext context, int position) {
              return getRow(context, position, currentUser);
            },
          )
      ),
    );
  }

  Widget getRow(BuildContext context, int rowIndex, User currentUser) {
    switch (rowIndex) {
      case 0:
        {
          if (currentUser == null) {
            return UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://avatarfiles.alphacoders.com/199/thumb-199068.jpg'),
              ),
              accountName: Text(
                  "Usuário Anonimo"
              ),
              accountEmail: Text(
                  "Seu e-mail"
              ),
            );
          }
          else {
            return UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://avatarfiles.alphacoders.com/199/thumb-199068.jpg'),
              ),
              accountName: Text(
                  "Planes Walker"
              ),
              accountEmail: Text(
                  currentUser.email
              ),
            );
          }
        }
        break;

      case 1:
        {
          return ListTile(
            leading: Icon(Icons.home),
            title: Text("Home"),
            subtitle: Text("Lista de Eventos"),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyApp()),
              );
            },
          );
        }
        break;

      case 2:
        {
          if(currentUser == null){
            return ListTile(
              leading: Icon(Icons.business_center),
              title: Text("Torneios"),
              subtitle: Text("Torneios"),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {

              },
            );
          }
          else {
           if (currentUser.email == "pedro_mathies@hotmail.com") {
              return ListTile(
                leading: Icon(Icons.business_center),
                title: Text("Página do Administrador"),
                subtitle: Text("Admin"),
                trailing: Icon(Icons.arrow_forward),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AdminPage()),
                  );
                },
              );
            }
           else{
             return ListTile(
               leading: Icon(Icons.business_center),
               title: Text("Torneios"),
               subtitle: Text("Torneios"),
               trailing: Icon(Icons.arrow_forward),
               onTap: () {

               },
             );
           }
          }
        }
        break;

      case 3:
        {
          if (currentUser == null) {
            return ListTile(
              leading: Icon(Icons.add_box),
              title: Text("Cadastre-se"),
              subtitle: Text("Adicionar Usuário"),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UserRegisterFirst()),
                );
              },
            );
          }
          else {
            return ListTile(
              leading: Icon(Icons.highlight_off),
              title: Text("Sign Out"),
              subtitle: Text("Log Off"),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                showAlertDialogSignOut(context);
              },
            );
          }
        }
        break;
      case 4:
        {
          if (currentUser == null) {
            return ListTile(
              leading: Icon(Icons.assignment_ind),
              title: Text("Log In"),
              subtitle: Text("Autenticar usuário"),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => userSignIn()),
                );
              },
            );
          }
        }
        break;
      default:
        {
          //statements;
        }
        break;
    }
  }

  showAlertDialogSignOut(context) {
    Widget simButton = FlatButton(
      child: Text("Sim"),
      onPressed: () {
        FirebaseAuth.instance.signOut();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MyApp()),
        );
      },
    );
    Widget naoButton = FlatButton(
      child: Text("Voltar"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    AlertDialog alerta = AlertDialog(
      title: Text("Fazer Log Out?"),
      actions: [
        simButton,
        naoButton
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }
}